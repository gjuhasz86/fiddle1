

import java.util.List;
import java.util.Random;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class PersonDb {
  private int calls = 0;
  private Random random = new Random(31337L);

  public Person getById(Integer key) {
    ++calls;
    return new Person(key, Address.getRandomAddresses(random));
  }

  public List<Person> getByIds(List<Integer> ids) {
    ++calls;
    return ids
        .stream()
        .map((id) -> new Person(id, Address.getRandomAddresses(random)))
        .collect(Collectors.toList());
  }

  public int getCalls() {
    return calls;
  }

  public void setCalls(int calls) {
    this.calls = calls;
  }

  public Collector<Integer, ?, Stream<Person>> getByidBatch() {
    return Collectors.collectingAndThen(Collectors.<Integer> toList(),
        result -> this.getByIds(result).stream());
  }
}