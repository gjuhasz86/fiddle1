

import java.util.List;

public class Person {
  private final int id;
  private final List<Address> addresses;

  public Person(int id, List<Address> addresses) {
    super();
    this.id = id;
    this.addresses = addresses;
  }

  public int getId() {
    return id;
  }

  public List<Address> getAddresses() {
    return addresses;
  }

  @Override
  public String toString() {
    return "Person [id=" + id + ", addresses=" + addresses + "]";
  }

}