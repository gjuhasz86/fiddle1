

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import lombok.Value;

@Value
public class Address {
  private int zipCode;

  public static List<Address> getRandomAddresses(Random random) {
    return random//
        .ints(3, 0, 10000)
        .boxed()
        .map(Address::new)
        .collect(Collectors.toList());
  }
}
