

import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {

  public static void main(String[] args) {
    new Main().main();
  }

  private void main() {
    List<Integer> personIds = getRandomIds();
    noBatch(personIds);
    withBatch(personIds);
  }

  private void noBatch(List<Integer> personIds) {
    PersonDb personDb = new PersonDb();

    personIds
        .stream()
        .filter(isEven)
        .map(personDb::getById)
        .filter(hasZipCodeSmallerThan(5000))
        .forEach(System.out::println);

    System.out.println("Number of db calls: " + personDb.getCalls());

  }

  private void withBatch(List<Integer> personIds) {
    PersonDb personDb = new PersonDb();

    personIds
        .stream()
        .filter(isEven)
        .collect(personDb.getByidBatch())
        .filter(hasZipCodeSmallerThan(5000))
        .forEach(System.out::println);

    System.out.println("Number of db calls: " + personDb.getCalls());
  }

  private List<Integer> getRandomIds() {
    return new Random().//
        ints(10, 0, 50)
        .boxed()
        .collect(Collectors.toList());
  }

  private Predicate<Person> hasZipCodeSmallerThan(int limit) {
    return (Person p) -> p.getAddresses().stream().anyMatch(a -> a.getZipCode() < limit);
  }

  private Predicate<Integer> isEven = (i) -> i % 2 == 0;

}
